##################################################################################################
# The following tries to import a custom Dash module that is the regular Dash with specific configs 
# required in Tetras Lab.
# If it fails (meaning you are not running the code from a Tetras Lab instance), it fallbacks to
# regular Dash.
# More info about executing Dash in a Tetras Lab environment are provided in the wikis :
#     - for admins:
#       https://gitlab.com/tetras-lab/tetras-lab/-/wikis/admin_manual#allowing-dash-applications-in-tetras-lab
#     - for data scientists and othe notebook editors : 
#        https://gitlab.com/tetras-lab/tetras-lab/-/wikis/data_scientist_manual#using-dash-in-tetras-lab
##################################################################################################
try:
    from tetraslab.dash import Dash
except ModuleNotFoundError:
    from dash import Dash
    
import dash_bootstrap_components as dbc
from src.callbacks import register_callbacks


class Appli:
    def __init__(self, layout, data):
        app = Dash()
        app.layout = layout
        app.data = data
        register_callbacks(app)
        app.run_server(jupyter_mode="inline")
    