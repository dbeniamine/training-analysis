class Component:
    def __init__(self, data):
        self.data = data
        
    def build(self):
        raise Error("Not implemented")
