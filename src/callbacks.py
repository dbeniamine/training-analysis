from dash import Output, Input


def register_callbacks(app):
    @app.callback(
        Output(component_id="my-output", component_property="children"),
        Input(component_id="my-input", component_property="value")
    )
    def update_output_div(input_value):
        return f"Output: {input_value}"
    