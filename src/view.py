from src.components.window import Window


class View:
    def __init__(self, data):
        self.layout = Window(data).build()
        